from flask import make_response
import pdfkit

CSS_LOCATION = 'C:/Users/filip/PycharmProjects/TestGenerator/templates/css/pdfTemplate.css'

class PdfGenerator:

    def __init__(self, content):
        self.content = content

    def generatePdf(self):
        return self.createPdf(self.content)

    def createPdf(self, content):

        pdf = pdfkit.from_string(content, False, css=CSS_LOCATION)
        response = make_response(pdf)
        response.headers['Content-Type'] = 'application/pdf'
        response.headers['Content-Disposition'] = 'attachment; filename=test.pdf'
        response.headers['Content-Accept'] = 'application/pdf'
        return response


