from flask import render_template
import datetime

from services.QuestionQueries.QuestionQueries import QuestionQueries


class HtmlGenerator:

    def __init__(self, testInfo, headerInfo):
        self.testInfo = testInfo
        self.headerInfo = headerInfo
        self.file = []

    def createHtml(self, questions, group):
        self.addHeader(group)
        self.addContent(questions)
        self.addSpace()
        return self.file

    def generateAllGroups(self):
        questionQueries = QuestionQueries(self.testInfo)
        for i in range(0,self.testInfo.groupAmount):
            questions = questionQueries.createTest()
            self.createHtml(questions, i+1)
        return ''.join(self.file)

    def addHeader(self, group):
        self.file.append('<meta http-equiv="Content-type" content="text/html; charset=utf-8" />')
        header = render_template('pdf_header_template.html',
                                 subject=self.headerInfo.subject,
                                 section=self.headerInfo.section,
                                 group=group,
                                 pointsAmount=self.headerInfo.pointsAmount,
                                 estimatedTime=self.headerInfo.estimatedTime,
                                 date=datetime.datetime.now().date());
        self.file.append(header)

    def addContent(self, questions):
        index = 1
        for question in questions:
            renderedQuestion = render_template('pdf_template.html',
                                       id=index,
                                       content=question.content,
                                       pointsAmount =question.points_amount)
            self.file.append(renderedQuestion)
            index = index +1

    def addSpace(self):
        space = '</br>'
        for spaceNumber in range(0, 4):
            self.file.append(space)