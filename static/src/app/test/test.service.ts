import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TestInfo } from './test-info';
import { environment } from '../../environments/environment';
import { Section } from '../section/section';
import { Observable } from 'rxjs/Observable';
import {Headers, Http, HttpModule, ResponseContentType} from "@angular/http";
import {TokenService} from "../login/services/token.service";

@Injectable()
export class TestService {

  constructor(private http: Http,
              private tokenService: TokenService) {
  }

  postTestInfo(testInfo: TestInfo){

    return this.http.post(environment.testGeneratorApi + '/getTestInfo', testInfo, {
      responseType: ResponseContentType.Blob,
      headers: this.tokenService.returnHeaders()
    })
  }

  getSections(){
    return this.http.get(environment.testGeneratorApi + '/getAllSections', {headers: this.tokenService.returnHeaders()})
  }


}
