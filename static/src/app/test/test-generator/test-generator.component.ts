import { Component, OnInit } from '@angular/core';
import { TestInfo } from '../test-info';
import { TestService } from '../test.service';
import { Router } from '@angular/router';
import { SectionService } from '../../section/section.service';
import { Section } from '../../section/section';
import { NgForm } from '@angular/forms';
import { saveAs } from 'file-saver';
import * as FileSaver from 'file-saver';
import {ResponseContentType} from "@angular/http";


@Component({
  selector: 'app-test-generator',
  templateUrl: './test-generator.component.html',
  providers: [TestService, SectionService]
})
export class TestGeneratorComponent implements OnInit {

  testModel: TestInfo;
  sectionList: any;
  param: TestInfo;

  constructor(private testService: TestService,
              private sectionService: SectionService,
              private router: Router) {
    this.testModel = new TestInfo();
  }

OnSubmit() {
  console.log(this.testModel);

  this.testService.postTestInfo(this.testModel).subscribe(
    (res) => {
      FileSaver.saveAs(new Blob([res.blob()], {type: 'application/pdf'}), 'test.pdf')
    },
      error => console.log(error)
    );
}

  ngOnInit() {
    this.sectionService.getSections().subscribe(sections => this.sectionList = sections);
  }
}
