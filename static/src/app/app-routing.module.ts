import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import { QuestionsListComponent } from './question/questions-list/questions-list.component';
import { AddQuestionComponent } from './question/add-question/add-question.component';
import { TestGeneratorComponent } from './test/test-generator/test-generator.component';
import { PageNotFoundComponent } from './helpers/page-not-found/page-not-found.component';
import {LoginComponent} from "./login/login.component";
import {RegisterComponent} from "./login/register/register.component";
import {LoginRedirect} from "./login/services/login-redirect.service";
import {EnsureAuthenticated} from "./login/services/ensure-authenticated.service";
import {LogoutComponent} from "./login/logout/logout.component";
import { UserQuestionListComponent } from './question/user-question-list/user-question-list.component';

const routes: Routes = [
  { path: '',  component: TestGeneratorComponent, canActivate: [EnsureAuthenticated] },
  { path: 'getAllQuestions', component: QuestionsListComponent, canActivate: [EnsureAuthenticated]  },
  { path: 'addQuestion', component: AddQuestionComponent, canActivate: [EnsureAuthenticated]  },
  { path: 'testInfo', component: TestGeneratorComponent, canActivate: [EnsureAuthenticated]  },
  { path: 'logout', component: LogoutComponent, canActivate: [EnsureAuthenticated] },
  { path: 'login', component: LoginComponent, canActivate: [LoginRedirect] },
  { path: 'register', component: RegisterComponent, canActivate: [LoginRedirect] },
  { path: '**', component: PageNotFoundComponent },
  { path: 'getUserQuestions', component: UserQuestionListComponent, canActivate: [EnsureAuthenticated]  }

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
