import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { QuestionsListComponent } from './question/questions-list/questions-list.component';
import { AppRoutingModule } from './/app-routing.module';
import { AddQuestionComponent } from './question/add-question/add-question.component';
import { FormsModule } from '@angular/forms';
import { NaviBarComponent } from './page-elements/navi-bar/navi-bar.component';
import { TestGeneratorComponent } from './test/test-generator/test-generator.component';
import { PageNotFoundComponent } from './helpers/page-not-found/page-not-found.component';
import { SectionComponent } from './section/section.component';
import { LoginComponent } from './login/login.component';
import {HttpModule} from "@angular/http";
import {AuthService} from "./login/services/auth.service";
import { RegisterComponent } from './login/register/register.component';
import {LoginRedirect} from "./login/services/login-redirect.service";
import {EnsureAuthenticated} from "./login/services/ensure-authenticated.service";
import { LogoutComponent } from './login/logout/logout.component';
import {UserService} from "./login/services/user-service.service";
import {TokenService} from "./login/services/token.service";
import { UserQuestionListComponent } from './question/user-question-list/user-question-list.component';

@NgModule({
  declarations: [
    AppComponent,
    NaviBarComponent,
    TestGeneratorComponent,
    QuestionsListComponent,
    PageNotFoundComponent,
    SectionComponent,
    AddQuestionComponent,
    LoginComponent,
    RegisterComponent,
    LogoutComponent,
    UserQuestionListComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    AppRoutingModule,
  ],
  providers: [
    AuthService,
    LoginRedirect,
    EnsureAuthenticated,
    UserService,
    TokenService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
