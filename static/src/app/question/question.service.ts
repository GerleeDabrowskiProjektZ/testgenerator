import { Injectable } from '@angular/core';
import {Question} from './question';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import {Headers, RequestOptions} from "@angular/http";
import {TokenService} from "../login/services/token.service";

@Injectable()
export class QuestionService {

  constructor(private http: HttpClient,
              private tokenService: TokenService) { }

  body: string;
  question: Question;

  postQuestion(question: Question){

    this.body = JSON.stringify(question);
    return this.http.post('http://localhost:5000' + '/insertNewQuestion', this.body, {headers: this.tokenService.returnHtmlHeaders()}).subscribe();
  }

  getQuestions(): Observable<Question[]>{

    return this.http.get<Question[]>(environment.testGeneratorApi + '/getAllQuestions', {headers: this.tokenService.returnHtmlHeaders()})
  }

  getUserQuestions(): Observable<Question[]>{
    return this.http.get<Question[]>(environment.testGeneratorApi + '/getUserQuestions', {headers: this.tokenService.returnHtmlHeaders()})
  }
}
