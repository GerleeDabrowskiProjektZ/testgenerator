import { Component, OnInit } from '@angular/core';
import { QuestionService } from '../question.service';
import { Question } from '../question';

@Component({
  selector: 'app-questions-list',
  templateUrl: './questions-list.component.html',
  providers: [QuestionService]
})
export class QuestionsListComponent implements OnInit {

  questionList: Question[] = [];
  constructor(private questionService: QuestionService) { }

  ngOnInit() {
    this.questionService.getQuestions().subscribe(question => this.questionList = question);
  }
} 