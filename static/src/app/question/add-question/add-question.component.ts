import { Component, OnInit } from '@angular/core';
import { Question } from '../question';
import { QuestionService } from '../question.service';
import { Router } from '@angular/router/';
import { NgForm} from '@angular/forms';
import { SectionService } from '../../section/section.service';
import { Section } from '../../section/section';

@Component({
  selector: 'app-add-question',
  templateUrl: './add-question.component.html',
  providers: [QuestionService, SectionService]
})
export class AddQuestionComponent implements OnInit {

  questionModel: Question;
  sectionList: any;
  

  constructor(private questionService: QuestionService,
              private sectionService: SectionService,
              private router: Router) { 
              this.questionModel = new Question();
  }

  OnSubmit(form: NgForm){
    console.log('submit Post click happend');
    this.questionModel.picture = '0';
    this.questionModel.table = '0';
    this.questionService.postQuestion(form.value);
    console.log(this.questionModel);
  }

  onAddToCollection() {
    alert('Pytanie zostalo dodane.');
   }

   ngOnInit() {
    this.sectionService.getSections().subscribe(sections => this.sectionList = sections);
  }
}
