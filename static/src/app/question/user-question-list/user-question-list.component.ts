import { Component, OnInit } from '@angular/core';
import { QuestionService } from '../question.service';
import { Question } from '../question';

@Component({
  selector: 'app-user-questions-list',
  templateUrl: './user-question-list.component.html',
  providers: [QuestionService]
})
export class UserQuestionListComponent implements OnInit {

  questionList: Question[] = [];
  constructor(private questionService: QuestionService) { }

  ngOnInit() {
    this.questionService.getUserQuestions().subscribe(question => this.questionList = question);
  }

  onEdit(){
    console.log('');
  }
}