import { Injectable } from '@angular/core';
import {Headers} from "@angular/http";
import {HttpHeaders} from "@angular/common/http";

@Injectable()
export class TokenService {

  constructor() { }

  returnHeaders(): Headers {
    const token = localStorage.getItem('token');
    return new Headers({
      'Authorization': `${token}`
    });
  }

  returnHtmlHeaders(): HttpHeaders {
    const token = localStorage.getItem('token');
    return new HttpHeaders({
      'Authorization': `${token}`
    });
  }


}
