import { Component, OnInit } from '@angular/core';
import {AuthService} from "../services/auth.service";
import {Router} from "@angular/router";
import {User} from "../models/user";
import {UserService} from "../services/user-service.service";

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
})
export class LogoutComponent implements OnInit {

  user: User = new User();

  constructor(private auth: AuthService,
              private router: Router,
              private userService: UserService) { }

  ngOnInit(): void  {
    const token = localStorage.getItem('token');
    this.auth.logout(token, this.userService.user)
      .then((user) => {
        localStorage.setItem('token', user.json().auth_token);
        localStorage.removeItem('token');
        this.router.navigateByUrl('/login');
      })
      .catch((err) => {
        console.log(err);
      });
  }

}
