import { Component, OnInit } from '@angular/core';
import {AuthService} from "./services/auth.service";
import {User} from "./models/user";
import {Router} from "@angular/router";
import {UserService} from "./services/user-service.service";

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent{

  user: User = new User();

  constructor(private auth: AuthService,
              private router: Router,
              private userService: UserService) {

  }

  onLogin(): void {
    this.auth.login(this.user)
    .then((user) => {
      localStorage.setItem('token', user.json().auth_token);
      this.userService.user = user;
      this.router.navigateByUrl('/');
    })
    .catch((err) => {
      console.log(err);
    });
  }

  btnClick = function () {
    this.router.navigateByUrl('/register');
};
}


