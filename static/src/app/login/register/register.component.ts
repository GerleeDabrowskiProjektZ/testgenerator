import { Component, OnInit } from '@angular/core';
import {User} from "../models/user";
import {AuthService} from "../services/auth.service";
import {Router} from '@angular/router';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  user: User = new User();

  constructor(private auth: AuthService,
              private router: Router) {

  }

  onRegister(): void {
    this.auth.register(this.user)
      .then((user) => {
        localStorage.setItem('token', user.json().auth_token);
        this.router.navigateByUrl('/');
      })
      .catch((err) => {
        console.log(err);
      });
  }
}
