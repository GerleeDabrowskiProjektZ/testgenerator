import { Component, OnInit } from '@angular/core';
import { SectionService } from './section.service';
import { Section } from './section';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { saveAs } from 'file-saver';
import * as FileSaver from 'file-saver';
import {ResponseContentType} from "@angular/http";



@Component({
  selector: 'app-section',
  templateUrl: './section.component.html',
  providers: [SectionService]
})
export class SectionComponent {

  sectionList: any;

  constructor(private sectionService: SectionService, private router: Router) { }

}
