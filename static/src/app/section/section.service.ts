import { Injectable } from '@angular/core';
import { Section } from './section';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import {TokenService} from "../login/services/token.service";

@Injectable()
export class SectionService {

  constructor(private http: HttpClient,
              private tokenService: TokenService) { }

  getSections(): Observable<Section[]>{
    return this.http.get<Section[]>(environment.testGeneratorApi + '/getAllSections', {headers: this.tokenService.returnHtmlHeaders()});
  }
}
