from flask import Blueprint, request, json, make_response, jsonify
from HtmlGenerator import HtmlGenerator
from PdfGenerator import PdfGenerator
from models.DbModels import User
from models.HeaderData import HeaderData
from models.TestInfo import TestInfo
from services.QuestionQueries.SectionQueries import SectionQueries

test_info_service = Blueprint('testInfoService', __name__, template_folder='services')

@test_info_service.route('/getTestInfo', methods=['GET', 'POST'])
def getInfo():
    auth_token = request.headers.get('Authorization')
    resp = User.decode_auth_token(auth_token)
    if isinstance(resp, str):
        responseObject = {
            'status': 'fail',
            'message': 'Provide a valid auth token.'
        }
        return make_response(jsonify(responseObject)), 401
    else:
        data = request.data
        dataJson = json.loads(data)

        testInfo = TestInfo(int(dataJson.get('id_sections')),
                            dataJson.get('testType'),
                            int(dataJson.get('points')),
                            int(dataJson.get('classLevel')),
                            int(dataJson.get('groupAmount')))

        sectionQuery = SectionQueries(testInfo.section)
        headerInfo = HeaderData('Biologia', sectionQuery.getSectionsNameById(), testInfo.groupAmount, testInfo.points, 5)
        htmlGenerator = HtmlGenerator(testInfo, headerInfo)
        result = htmlGenerator.generateAllGroups()
        pdfGenerator = PdfGenerator(result)

        return pdfGenerator.generatePdf()



