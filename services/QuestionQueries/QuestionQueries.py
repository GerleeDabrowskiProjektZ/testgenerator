from models.Questions import Questions
import random


class QuestionQueries:
    def __init__(self, testInfo):
        self.testInfo = testInfo
        self.option = {
            5: self.createFivePointsTest,
            10: self.createTenPointsTest,
            15: self.createFifteenPointsTest,
            20: self.createTwentyPointsTest
        }

    def getQuestionsByPointsAndSection(self, points_amount):
        query = Questions.query.filter_by(section_id=self.testInfo.section, points_amount=points_amount)
        onePointsQuestions = query.all()
        return onePointsQuestions

    def draw(self, points, amount):
        allItems = self.getQuestionsByPointsAndSection(points)
        return random.sample(allItems, amount)

    def createTest(self):
        return self.option[self.testInfo.points]()

    def createFivePointsTest(self):
        onePointsQuestions = self.draw(1, 3)
        twoPointsQuestions = self.draw(2, 1)
        questionList = onePointsQuestions + twoPointsQuestions
        return questionList

    def createTenPointsTest(self):
        onePointsQuestions = self.draw(1, 3)    # 3 x 1 point
        twoPointsQuestions = self.draw(2, 2)    # 2 x 2points
        threePointsQuestions = self.draw(3, 1)  # 1 x 3 points
        questionList = onePointsQuestions + twoPointsQuestions + threePointsQuestions
        return questionList

    def createFifteenPointsTest(self):
        onePointsQuestions = self.draw(1, 3)    # 3 x 1 point
        twoPointsQuestions = self.draw(2, 3)    # 3 x 2 points
        threePointsQuestions = self.draw(3, 2)  # 2 x 3 points
        questionList = onePointsQuestions + twoPointsQuestions + threePointsQuestions
        return questionList

    def createTwentyPointsTest(self):
        onePointsQuestions = self.draw(1, 3)    # 3 x 1 point
        twoPointsQuestions = self.draw(2, 4)    # 4 x 2 points
        threePointsQuestions = self.draw(3, 3)  # 3 x 3 points
        questionList = onePointsQuestions + twoPointsQuestions + threePointsQuestions
        return questionList
