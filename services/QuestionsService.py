from flask_jwt import jwt_required
from flask_security import auth_token_required

from models.DbModels import User
from models.Questions import Questions
from flask import jsonify, Blueprint, request, json, make_response
from DataBase import db

question_service = Blueprint('questionService', __name__, template_folder='services')


@question_service.route('/getAllQuestions')
def getAllQuestions():
    auth_token = request.headers.get('Authorization')
    resp = User.decode_auth_token(auth_token)
    if isinstance(resp, str):
        responseObject = {
            'status': 'fail',
            'message': 'Provide a valid auth token.'
        }
        return make_response(jsonify(responseObject)), 401
    else:
        allQuestions = db.session.query(Questions).all()
        return jsonify([item.serialize for item in allQuestions])

@question_service.route('/getUserQuestions')
def getUserQuestions():
    auth_token = request.headers.get('Authorization')
    resp = User.decode_auth_token(auth_token)
    if isinstance(resp, str):
        responseObject = {
            'status': 'fail',
            'message': 'Provide a valid auth token.'
        }
        return make_response(jsonify(responseObject)), 401
    else:
        userQuestions = Questions.query.filter_by(creator_id=5).all()
        return jsonify([item.serialize for item in userQuestions])

@question_service.route('/insertNewQuestion',  methods=['GET', 'POST'])
def insertNewQuestion():
    auth_token = request.headers.get('Authorization')
    id = User.decode_auth_token(auth_token)
    if isinstance(id, str):
        responseObject = {
            'status': 'fail',
            'message': 'Provide a valid auth token.'
        }
        return make_response(jsonify(responseObject)), 401
    else:
        data = request.data
        dataJson = json.loads(data)
        question = Questions(class_level=dataJson.get('classLevel'),
                             points_amount=dataJson.get('question-points-amount'),
                             content=dataJson.get('question-content'),
                             type=dataJson.get('question-type'),
                             picture=dataJson.get('question-picture'),
                             table=dataJson.get('question-table'),
                             time=dataJson.get('question-time'),
                             section_id=dataJson.get('id_sections'),
                             creator_id=id)

        db.session.add(question)
        db.session.commit()
        db.session.flush()
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@question_service.route('/updateQuestion',  methods=['GET', 'POST'])
def updateQuestion():
    auth_token = request.headers.get('Authorization')
    id = User.decode_auth_token(auth_token)
    if isinstance(id, str):
        responseObject = {
            'status': 'fail',
            'message': 'Provide a valid auth token.'
        }
        return make_response(jsonify(responseObject)), 401
    else:
        data = request.data
        dataJson = json.loads(data)
        question = dict(
                         id=dataJson.get('question-id'),
                         class_level=dataJson.get('question-classLevel'),
                         points_amount=dataJson.get('question-classLevel'),
                         content=dataJson.get('question-content'),
                         type=dataJson.get('question-type'),
                         picture=dataJson.get('question-picture'),
                         table=dataJson.get('question-table'),
                         time=dataJson.get('question-time'),
                         section_id=dataJson.get('section-number'),
                         creator_id_id=id)

        Questions.query.filter_by(id=1).update(question)
        db.session.commit()
        db.session.flush()
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@question_service.route('/delete',  methods=['GET', 'POST'])
def delete():
    auth_token = request.headers.get('Authorization')
    id = User.decode_auth_token(auth_token)
    if isinstance(id, str):
        responseObject = {
            'status': 'fail',
            'message': 'Provide a valid auth token.'
        }
        return make_response(jsonify(responseObject)), 401
    else:
        data = request.data
        dataJson = json.loads(data)
        id = dataJson.get('question-id')
        question = Questions.query.filter_by(id=id).first()
        db.session.delete(question)
        db.session.commit()
        db.session.flush()
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}