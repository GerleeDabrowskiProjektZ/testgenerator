from flask_login import login_required

from models.DbModels import User
from models.Sections import Sections
from flask import jsonify, Blueprint, request, make_response
from DataBase import db

section_service = Blueprint('sectionService', __name__, template_folder='services')

@section_service.route('/getAllSections')
def getAllSections():
    auth_token = request.headers.get('Authorization')
    resp = User.decode_auth_token(auth_token)
    if isinstance(resp, str):
        responseObject = {
            'status': 'fail',
            'message': 'Provide a valid auth token.'
        }
        return make_response(jsonify(responseObject)), 401
    else:
        allSections = db.session.query(Sections).all()
        return jsonify([item.serialize for item in allSections])

@section_service.route('/getSectionName')
def getSectionsNameById(id):
    auth_token = request.headers.get('Authorization')
    resp = User.decode_auth_token(auth_token)
    if isinstance(resp, str):
        responseObject = {
            'status': 'fail',
            'message': 'Provide a valid auth token.'
        }
        return make_response(jsonify(responseObject)), 401
    else:
        section = Sections.query.filter_by(id_sections=id).first()
        return section.section_name