
class HeaderData:
    def __init__(self, subject, section, group, pointsAmount, estimatedTime):
        self.subject = subject
        self.section = section
        self.group = group
        self.pointsAmount = pointsAmount
        self.estimatedTime = estimatedTime