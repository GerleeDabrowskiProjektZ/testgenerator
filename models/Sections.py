from DataBase import db

class Sections(db.Model):
    __tablename__ = 'sections'
    id_sections = db.Column(db.Integer, primary_key=True)
    section_name = db.Column(db.String(45), unique=True)

    @property
    def serialize(self):
        return {
            'id_sections': self.id_sections,
            'section_name': self.section_name,
        }