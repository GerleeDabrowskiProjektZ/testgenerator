from DataBase import db
from sqlalchemy import ForeignKey

class Questions(db.Model):
    __tablename__ = 'questions'
    id = db.Column(db.Integer, primary_key=True)
    class_level = db.Column(db.Integer)
    points_amount = db.Column(db.Integer)
    content = db.Column(db.String(45), unique=True)
    type = db.Column(db.String(45))
    picture = db.Column(db.String(45))
    table = db.Column(db.String(45))
    time = db.Column(db.Integer)
    section_id = db.Column(db.Integer, ForeignKey('sections.id_sections'))
    creator_id = db.Column(db.Integer, ForeignKey('users.id'))

    @property
    def serialize(self):
        return {
            'id': self.id,
            'content': self.content,
            'class_level': self.class_level,
            'points_amount': self.points_amount,
            'type': self.type,
            'picture': self.picture,
            'table': self.table,
            'time': self.time,
            'creator_id': self.creator_id,
        }