from flask import Flask
from flask_security import Security

from services.Views import auth_service
from services.SectionsService import section_service
from services.QuestionsService import question_service
from DataBase import db, DATABASE_CONFIGURATION, dbUser, bcrypt
from services.TestinfoService import test_info_service
from flask_cors import CORS

app = Flask(__name__)

CORS(app, expose_headers=['Content-Disposition', 'Content-Type', 'Authorization', 'Authentication-Token'], allow_headers=['Content-Disposition', 'Content-Type', 'Authorization', 'Authentication-Token'])
app.config['SQLALCHEMY_DATABASE_URI'] = DATABASE_CONFIGURATION #user,password,database name
app.config['SECRET_KEY'] = 'my_precious'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECURITY_REGISTERABLE'] = True
app.config['SECURITY_PASSWORD_HASH'] = 'plaintext'
app.config['WTF_CSRF_ENABLED'] = False
app.config['JSON_AS_ASCII'] = False
app.config['JWT_DEFAULT_REALM'] = 'Login Required'
app.config['CORS_HEADERS'] = 'Content-Type'
app.config['BCRYPT_LOG_ROUNDS'] = 12
app.debug = True
app.register_blueprint(section_service)
app.register_blueprint(question_service)
app.register_blueprint(test_info_service)
app.register_blueprint(auth_service)
app.config.get('BCRYPT_LOG_ROUNDS')

security = Security()
security.init_app(app)
db.init_app(app)
dbUser.init_app(app)
bcrypt.init_app(app)

@app.before_first_request
def create_user():
    db.create_all()

if __name__ == '__main__':
    app.run()

