-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 26 Sty 2018, 22:54
-- Wersja serwera: 10.1.28-MariaDB
-- Wersja PHP: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `test_generator`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `blacklist_tokens`
--

CREATE TABLE `blacklist_tokens` (
  `id` int(11) NOT NULL,
  `token` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `blacklisted_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `blacklist_tokens`
--

INSERT INTO `blacklist_tokens` (`id`, `token`, `blacklisted_on`) VALUES
(1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MTcwMDY3MTgsImlhdCI6MTUxNjkyMDMxOCwic3ViIjoxfQ.kJeF7SHg4Y_ONBmqLX3ealVqD8IePqjgUNRkhsP9kFI', '2018-01-26 00:06:30'),
(2, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MTcwMDgwOTMsImlhdCI6MTUxNjkyMTY5Mywic3ViIjoxfQ.5CzGP0Ki7hvauQtbDn5NkZ6icQ_nRw02TGRJlMF6TN0', '2018-01-26 00:08:42'),
(3, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MTcwMDgxMzMsImlhdCI6MTUxNjkyMTczMywic3ViIjoxfQ.r-7IdJqogf_bEhnXV7R9tDWgvACsXEeh80BrNig01k0', '2018-01-26 00:59:46'),
(4, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MTcwMTEyMjMsImlhdCI6MTUxNjkyNDgyMywic3ViIjoxfQ.NM04YjEotSj10YZazleoh_nuh2oUoyslUCjTbqf5Bj8', '2018-01-26 19:15:03'),
(5, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MTcwNzY5MDcsImlhdCI6MTUxNjk5MDUwNywic3ViIjoxfQ.lZeb2jD9x4LcbAcC6_LLZe-h_RJGf4MeBH-AK5fkrA8', '2018-01-26 20:28:17');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `class_level` int(11) NOT NULL,
  `points_amount` int(11) NOT NULL,
  `content` longtext COLLATE utf8_polish_ci NOT NULL,
  `type` varchar(45) COLLATE utf8_polish_ci NOT NULL,
  `picture` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  `table` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  `time` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `creator_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `questions`
--

INSERT INTO `questions` (`id`, `class_level`, `points_amount`, `content`, `type`, `picture`, `table`, `time`, `section_id`, `creator_id`) VALUES
(1, 2, 2, 'Uzupełnij podane zdania odpowiednimi nazwami metod rozumowania:  A-……………… to formułowanie twierdzeń szczegółoych na podstawie uogólnień , B-……………..to formułowanie uogólnień na podstawie wiedzy szczegółowej.', 'otwarte', '0', '0', 2, 2, 1),
(2, 2, 2, 'Uporządkuj  schemat pozyskiwania oraz opracowania wyników zebranych  podczas badań biologicznych:  a. sformułowanie wniosku  b. weryfikacja hipotezy  c. sformułowanie problemu badawczego  d. postawienie hipotezy  e. publikowanie  danych w czasopismach', 'zamknięte', '', '', 2, 1, 1),
(3, 2, 2, 'Wyjaśnij czym różni się obserwacja od eksperymentu.', 'otwarte', '', '', 2, 1, 1),
(4, 2, 3, 'Określ czy poniższe zdania są prawdziwe( P) czy fałszywe(F)                                               a. Układ optyczny mikroskopu służy do oświetlenia preparatu……  b. Obraz  spod mikroskopu jest pozorny, powiększony i odwrócony…….  C. Zdolność rozdzielcza mikroskopu to największa odległość  między dwoma punktami obiektu pod mikroskopem .....', 'zamknięte', '', '', 2, 1, 1),
(5, 2, 1, 'Określ jakich przyrządów użyjesz do obserwacji  krwinek czerwonych oraz wirusów.', 'otwarte', '', '', 1, 1, 1),
(6, 2, 3, 'Wyjaśnij  w jakich przypadkach stosuje się poniższe sposoby dokumentowania badań:   a. tabela  b. wykres liniowy  c. film', 'otwarte', '', '', 2, 1, 1),
(7, 2, 2, 'Podaj cztery przykłady zastosowań mikroskopu elektronowego.', 'otwarte', '', '', 2, 1, 1),
(8, 2, 3, 'Określ funkcje poszczególnych elementów układu optycznego i mechanicznego w mikroskopie optycznym', 'otwarte', '', '', 4, 1, 1),
(9, 2, 2, 'Wyjaśnij co to jest próba kontrolna i próba badawcza w doświadczeniu.', 'otwarte', '', '', 1, 1, 1),
(10, 2, 1, 'Podaj kryterium według którego wyróżnia się makro- i mikroelementy.', 'otwarte', '', '', 1, 2, 1),
(11, 2, 1, 'Podkreśl te z wymienionych pierwiastkow, które nie są makroelementami : azot, żelazo, fosfor, magnes, potas, siarka, sód, tlen, wapń, węgiel, wodór, jod.', 'zamknięte', '', '', 1, 2, 1),
(12, 2, 1, 'Wymień wszystkie pierwiastki biogenne, które wchodzą w skład węglowodanów.', 'otwarte', '', '', 1, 2, 1),
(13, 2, 1, 'Podaj cechę wspólną wszystkich pierwiastków biogennych.', 'otwarte', '', '', 1, 2, 1),
(14, 2, 3, 'Bazyliszek płatkogłowy świetnie pływa i nurkuje. Może również biegać po powierzchni wody. A. podaj cechę wody, która umożliwia zwierzęciu utrzymanie się na jej powierzchni B. wyjaśnij jakie właściwości chemiczne cząsteczek wody warunkują tę możliwość. C. Podaj inną niż w podpunkcie a cechę wody, która również wynika z jejwłaściwości chemicznych.', 'otwarte', '', '', 4, 2, 1),
(15, 2, 3, 'Oceń czy zdania śą prawdziwe (P) czy fałszywe(F) : A Wiązanie wodorowe jest silne, jego obecność daje efekt kohezji między cząsteczkami wody. B. Wiązanie kowalencyjne w wodzie jest spolaryzowane, przez co cząsteczki wody są dipolami. C. Duże napięcie powierzchniowe wody jest efektem występowania wiązań wodorowych między cząsteczkami wody. D. Woda ma wysokie ciepło parowania, przy niewielkiej ilości energii może zmienić stan skupienia', 'zamknięte', '', '', 4, 2, 1),
(16, 2, 1, 'Wyjaśnij z której właściwości wody korzysta maratończyk polewając ciało wodą podczas biegu', 'otwarte', '', '', 1, 2, 1),
(17, 2, 2, ' określ znaczenie zjawiska adhezji i kohezji dla organizmów.', 'otwarte', '', '', 2, 2, 1),
(18, 2, 1, 'Podaj po jednym przykładzie roli soli mineralnych występujących w organizmie w postaci rozpuszczonej i nierozpuszczonej.', 'otwarte', '', '', 1, 2, 1),
(19, 2, 2, 'Podaj po dwie funkcje biologiczne wapnia i  magnezu.', 'otwarte', '', '', 2, 2, 1),
(20, 2, 1, 'Objawem niedoboru jakiego pierwiastka jest- ,, osłabienie i anemia, zaburzenia rytmu pracy serca, zakłócenie procesu oddychania wewnątrzkomórkowego.\"', 'otwarte', '', '', 1, 2, 1),
(21, 2, 1, 'Objawem niedoboru jakiego pierwiastka jest- ,, powiększenie tarczycy (wole), obrzęki skóry, niedorozwój umysłowy.', 'otwarte', '', '', 1, 2, 1),
(22, 2, 1, 'Podaj kryterium podziału sacharydów na mono-, oligo-, polisacharydy.', 'otwarte', '', '', 1, 2, 1),
(23, 2, 1, 'Które z wymienionych związków chemicznych nie są monosacharydami: aldehyd glicerynowy, sacharoza, ryboza, fruktoza, galaktoza, maltoza.', 'otwarte', '', '', 1, 2, 1),
(24, 2, 1, 'Wskaż zdanie prawidłowo określające rolę glukozy: A. jest wykorzystywana bezpośrednio przez organizmy jako substrat w procesie oddychania wewnątrkomórkowego. B. jest głównym materiałem zapasowym organizmu.', 'zamknięte', '', '', 1, 2, 1),
(25, 2, 1, 'które z wymienionych związków chemicznych  są monosacharydami: aldehyd glicerynowy, sacharoza, ryboza, fruktoza, galaktoza, maltoza, skrobia.', 'otwarte', '', '', 1, 2, 1),
(26, 2, 1, 'Wymień sacharydy , które składają się wyłącznie z cząsteczek glukozy.', 'otwarte', '', '', 1, 2, 1),
(27, 2, 1, 'Zaznacz te spośród podanych poniżej cząsteczek, które nie są rozpuszczalne w wodzie: fosfolipidy, glukoza, maltoza, lanolina, chityna, glikogen, chlorek sodu, cholesterol.', 'otwarte', '', '', 1, 2, 1),
(28, 2, 2, 'Polisacharydy odgrywają rolę strukturalną i zapasową u organizmów żywych. Wskaż zdania, które zawierają prawdziwe informacje o funkcji i strukturze polisacharydów: A. synteza skrobi umożliwia roślinom zmagazynowanie nadmiaru sacharozy. B. Glikogen jest polimerem zbudowanym z glukozy i ma strukturę podobną do amylopektyny. C. W celulozie i skrobi występują różne typy wiązań O-glikozydowych, w efekcie czego te cząsteczki mają różną strukturę przestrzenną. D. Chityna jest głównym składnikiem ścian komórkowych bakterii i grzybów i jest zbudowana z glukozaminy.', 'zamknięte', '', '', 2, 2, 1),
(29, 2, 2, 'Porównaj budowę chemiczną skrobi i glikogenu', 'otwarte', '', '', 1, 2, 1),
(30, 2, 2, 'Porównaj budowę chemiczną skrobi i  celulozy.', 'otwarte', '', '', 1, 2, 1),
(31, 2, 1, 'Podaj występowanie i znaczenie celulozy.', 'otwarte', '', '', 1, 2, 1),
(32, 2, 1, 'Wymień kryteria podziału tłuszczów.', 'otwarte', '', '', 1, 2, 1),
(33, 2, 1, 'Wyjaśnij, na czym polega różnica między tłuszczami nasyconymi i nienasyconymi.', 'otwarte', '', '', 1, 2, 1),
(34, 2, 1, 'Wskaż prawidłowe dokończenie zdania : Do lipidów złożonych nalężą: a. fosfolipidy i glikolipidy, b. fosfolipidy i woski.', 'zamknięte', '', '', 1, 2, 1),
(35, 2, 3, 'Oceń czy zdania są prawdziwe (P) czy fałszywe(F) :  A. W tłuszczach właściwych alkoholem jest glicerol, który ma trzy grupy hydroksylowe. B. Cholesterol wchodzi w skład błon biologicznych i osłonek mielinowych. C. Lipidy proste to produkty polimeryzacji cząsteczek izoprenu. D. Cholesterol jest substancją wyjściową do syntezy witaminy D.', 'zamknięte', '', '', 2, 2, 1),
(36, 2, 3, 'Poniżej wymieniono różne określenia dotyczące peptydów.                                                  (Wazopresyna, białka proste, kolagen, insulina, makropeptydy, peptydy, białka złożone, polipeptydy, oligopeptydy, hemoglobina.)  uporządkuj w formie grafu wymienione określenia. ', 'otwarte', '', '', 2, 2, 1),
(37, 2, 1, 'Wyjaśnij różnicę między polipeptydami a makropeptydami.', 'otwarte', '', '', 1, 2, 1),
(38, 2, 3, 'Oceń prawdziwość stwierdzeń dotyczących hemoglobiny P/F       A. w strukturze czwartorzędowej hemoglobiny występują cztery podjednostki białkowe. B. Pod wpływem wysokiej temperatury hemoglobina ulega denaturacji.  C. Struktura hemoglobiny zawiera wielopierścieniowy związek niebiałkowy. D. Hemoglobina jest makropeptydem.', 'otwarte', '', '', 2, 2, 1),
(39, 2, 2, 'Do padanych grup białek ( a-d) przyporządkuj podane przykłady białek ( I- IV)         a. białka strukturalne  b. białka regulujące  c. białka odpornościowe d. białka transportujące    I-hemoglobina  II-enzymy  III-kolagen  IV-immunoglobuliny', 'zamknięte', '', '', 2, 2, 1),
(40, 2, 2, 'Określ kryterium,według którego komórki dzieli się na prokariotyczne i eukariotyczne. Podaj przykłady organizmówzbudowanych z wymienionych rodzajów komórek.', 'otwarte', '', '', 1, 3, 1),
(41, 2, 2, 'Wymień różnice w budowie komórki prokariotycznej i eukariotycznej.', 'otwarte', '', '', 2, 3, 1),
(42, 2, 2, 'Wymień cechy wspólne komórek: roślinnej, zwierzęcej i roślinnej.', 'otwarte', '', '', 2, 3, 1),
(43, 2, 3, 'Przedstaw na schematycznym rysunku budowę błony komórkowe uwzględniając elementy zapewniające pełnienie funkcji ochronnych oraz związanych z transportem przez błony.', 'otwarte', '', '', 2, 3, 1),
(44, 2, 1, 'Wskaż funkcję, której nie pełnią błony biologiczne: a. tworzenie fizycznych przegród b. kontrola transportu cząsteczek c. synteza DNA  d. odbieranie sygnałów ze środowiska zewnętrznego', 'zamknięte', '', '', 1, 3, 1),
(45, 2, 1, 'Wskaż prwidłowe dokończenie zdania :  W komórkach wydzielniczych trzustki obserwuje się silnie rozbudowane : a. peroksysomy  b.wodniczki tętniące  c. struktury aparatu Golgiego  d. leukoplasty', 'zamknięte', '', '', 1, 3, 1),
(46, 2, 2, 'Przyporządkuj odpowiednie struktury (I, II) komórkowe do podanych funkcji (A-F).               I-przekształcenie lipidów w cukry      II- magazynowanie związków organicznych w liścieniach fasoli      a. peroksysomy b.chromoplasty c. leukoplsty d. mitochondria e. centriole  f. glioksysomy', 'zamknięte', '', '', 2, 3, 1),
(47, 2, 2, 'Oceń prawdziwość stwierdzeń dotyczących  połączeń międzykomórkowych:  A. Komórki roślinne kontaktują się za pomocą desmosomów  B- połączenia komunikacyjne zbudowane są z kompleksów białkowych przez które kontaktują się cytoplazmy sąsiadujących komórek  C-plazmodesmy występują u roślini są to cienkie pasma cytoplazmy przenikające z komórki do komórki  D-U zwierząt liczne połączenia komórkowe występują w nabłonkach wyściełających narządy wewnętrzne', 'zamknięte', '', '', 1, 3, 1),
(48, 2, 2, 'Przyporządkuj  rodzajom komórek( I, II) odpowiednie cechy rybosomów(a-e)                      I - komórka nabłonkowa jelita roślinożercy   II -pałeczka okrężnicy                                            a. są zbudowane z dwóch podjednostek  b. współczynnik ich sedymentacji wynosi 70S     c. są obecne np w mitochondriach  d. współczynnik ich sedymentacji wynosi 80S              e. są odpowiedzialne za syntezę białek', 'zamknięte', '', '', 2, 3, 1),
(49, 2, 1, 'Podział jądra komórkowego to: a-cytokineza b-interfaza c-kariokineza d-transformacja', 'zamknięte', '', '', 1, 3, 1),
(50, 2, 2, 'Zaznacz zdania, które dotyczą mejozy : a. w wyniku tego podziału powstają dwie komórki potomne  b. w wyniku tego podziału powstają komórki haploidalne  c. w jej efekcie powstają zarodniki  d. w wyniku tego podziału  powstają  cztery komórki potomne  e. w jej efekcie u roślin powstają komórki somatyczne  f. składa się z dwóch następujących po sobie podziałów.', 'zamknięte', '', '', 1, 3, 1),
(51, 2, 1, 'Wybierz prawidłowe zakończenie zdania: Płynność błony komórkowej zależy od  a. krótkich i sztywnychcząsteczek cholesterolu   b.  Obecności glikokaliksu na powierzchni błony', 'zamknięte', '', '', 1, 3, 1),
(52, 2, 1, ' Do białek błonowych nie należą :  a- receptory hormonów peptydowych  b- cyklaza adenylowa  c-spektryna  d-miozyna  e-pompa sodowo-potasowa  f- hemoglobina', 'zamknięte', '', '', 1, 3, 1),
(53, 2, 1, 'Najdłuższą fazą życia komórki jest a- metafaza  b-interfaza  c- faza M', 'zamknięte', '', '', 1, 3, 1),
(54, 2, 2, 'Wyjaśnij dlaczego mejoza jest źródłem zmienności organizmów. Podaj dwa argumenty', 'otwarte', '', '', 1, 3, 1),
(55, 2, 2, 'Wyjaśnij jakie znaczenie w przyrodzie ma mitoza,', 'otwarte', '', '', 1, 3, 1),
(56, 2, 2, 'Przedstaw różnice w przebiegu anafazy mitozy i anafazy mejozy I', 'otwarte', '', '', 2, 3, 1),
(57, 2, 3, 'Wykonaj rysunki ilustrujące przebieg metafazy I podziału mejotycznego dla komórki w której 2n=8', 'otwarte', '', '', 4, 3, 1),
(58, 2, 3, 'Wykonaj rysunki ilustrujące przebieg metafazy I podziału mejotycznego dla komórki w której 2n=4', 'otwarte', '', '', 4, 3, 1),
(59, 2, 3, 'Porównaj proces cytokinezy w komórce roślinnej i zwierzęcej', 'otwarte', '', '', 2, 3, 1),
(60, 2, 1, 'Podaj cechy wirusów świadczące o przynależności do świata materii nieożywionej', 'otwarte', '', '', 1, 4, 1),
(61, 2, 1, 'Podaj cechy wirusów świadczące o przynależności do świata materii ożywionej', 'otwarte', '', '', 1, 4, 1),
(62, 2, 1, 'Wymień elementy wirionu.', 'otwarte', '', '', 1, 4, 1),
(63, 2, 3, 'Wymień formy wirusów , przedstaw schematyczne ich rysunki i podaj przykłady.', 'otwarte', '', '', 4, 4, 1),
(64, 2, 3, 'Przedstaw sposoby rozprzestrzeniania się wirusowych  chorób roślin, zwierząt i człowieka.', 'otwarte', '', '', 2, 4, 1),
(65, 2, 2, 'Oceń czy zdania są prawdziwe czy fałszywe(P/F): A. cykllityczny wirusów kończy się rozpadem komórki gospodarza.  B. nieczynne fagowe DNA zintegrowane z DNA bakteryjnym nazywamy profagiem.  C. cykle lityczny i lizogeniczny bakteriofagów różnią się sposobem wnikania wirusowego DNA do komórki bakteryjnej.', 'zamknięte', '', '', 1, 4, 1),
(66, 2, 1, 'Nazwij opisaną chorobę : \" Wywołana jest przez prątki Kocha, może przenosić się drogą kropelkową, pokarmową  a w sporadycznych sytuacjach nawet drogą płciową. Bakteria może atakować rózne narządy.Objawami postaci płucnej mogą być kaszel , spadek masy ciała.', 'otwarte', '', '', 1, 4, 1),
(67, 2, 1, 'Nazwij opisaną chorobę : \" Wywołana jest przez  krętki, których nosicielami są pajęczaki   ( kleszcze). Po ukąszeniu przez zakażonego kleszcza u człowieka może się pojawić rumień wędrujący, objawy grypopodobne, osłabienie i objawy ze strony układu nerwowego.', 'otwarte', '', '', 1, 4, 1),
(68, 2, 2, 'Oceń prawdziwość stwierdzeń  odnoszących się do bakterii autotroficznych:                         A. Dla wszystkich bakterii żródłem energii wykorzystywanej w procesach syntezy jest promieniowanie słoneczne  B. Produktem ubocznym fotosyntezy zielonych i purpurowych bakterii siarkowych jest tlen  C.źródłem energii wykorzystywanej przez bakterie chemosyntetyzujące do syntezy związków organicznych jest utlenianie znajdujących się  w ich środowisku związków nieorganicznych', 'zamknięte', '', '', 1, 4, 1),
(69, 2, 2, 'przyporządkuj  każdej z wymienionych grup bakterii(I-III) jej źródło pokarmu(a-d)                      I- saprobionty  II-symbionty  III- pasożyty      a. pokarm czerpią z ciał innych organizmów, równocześnie wyrządzając im szkody  b. wykorzystują  jako pokarm martwą materię organiczną  c.przeprowadzają chemosyntezę  d.korzystają z substancji wytworzonych przez inne organizmy, w zamian dostarczają im substancje wytwarzane przez siebie.', 'zamknięte', '', '', 1, 4, 1),
(70, 2, 1, 'Wyjaśnij w jaki sposób można określić przynależność danej bakterii do grupy bakterii Gram+ lub Gram -', 'otwarte', '', '', 1, 4, 1),
(71, 2, 2, 'Oceń czy zdania są prawdziwe czy fałszywe (P/F) : A. w niekorzystnych warunkach środowiska bakterie zwiększają tempo metabolizmu i przechodzą w stan anabiozy.   B. cysty stanowią formę przetrwalnikową bakterii,powstałą przez odwodnienie i otoczenie grubą ścianą komórki bakteryjnej.  C. Wszystkie gatunki bakterii mają zdolność wytwarzania przetrwalników.', 'zamknięte', '', '', 1, 4, 1),
(72, 2, 2, 'Wykreśl z podanych zdań nieprawdiwe informacje, tak aby powstałe zdania były poprawne: A. Obecność otoczki śluzowej  wpływa/ nie wpływa na zjadliwość szczepów bakterii chorobotwórczych.  B. Cechą bakterii Gram + jest to, że po zastosowaniu metody Grama  trwale/ nietrwale  barwią się na kolor fioletowy.  C. Budowa ściany komorkowej bakterii decyduje/ nie decyduje o jej wrażliwości na działanie leków (np. antybiotyków)', 'zamknięte', '', '', 1, 4, 1),
(73, 2, 1, 'Wskaż choroby bakteryjne:  lamblioza,odra, grużlica, ospa, borelioza,  tężec, grypa, różyczka, cholera.', 'zamknięte', '', '', 1, 4, 1),
(74, 2, 2, 'Opisz dwie korzyści wynikające z obecności naturalnej flory bakteryjnej w jelitach.', 'otwarte', '', '', 1, 4, 1),
(75, 2, 3, 'Opisz budowę i funkcję elementów komórki bakteryjnej.', 'otwarte', '', '', 4, 4, 1),
(76, 2, 3, ' Przedstaw klasyfikację bakterii ze względu na sposób odżywiania się. Podaj po jednym przykładzie bakterii w każdej grupie.', 'otwarte', '', '', 4, 4, 1),
(77, 2, 2, 'Przedzstaw klasyfikację bakterii ze względu na sposób oddychania.Podaj po jednym przykładzie bakterii w każdej grupie.', 'otwarte', '', '', 2, 4, 1),
(78, 2, 1, 'Jaką rolę spełniają heterocysty w komórkach sinic Gleocapsa czy Nostoc?', 'otwarte', '', '', 1, 4, 1),
(79, 2, 2, 'Wymień sposoby rozmnamżania u bakterii i krótko opisz każdy', 'otwarte', '', '', 4, 4, 1),
(80, 2, 2, 'U niektórych bakterii i protistów zachodzi proces płciowy nazywany koniugacją. Określ, czy poniższe stwierdzenie jest prawdziwe dla obu wymienionych grup organizmów. Uzasadnij swoją odpowiedź.   \" W efekcie koniugacji oba organizmy uczestniczące w tym procesie mają identyczny materiał genetyczny\"', 'otwarte', '', '', 1, 4, 1),
(81, 2, 3, 'Podaj trzy sposoby bezpłciowego rozmnażania się grzybów i krtko je scharakteryzuj.', 'otwarte', '', '', 2, 4, 1),
(82, 2, 2, 'Oceń czy zdania są prawdziwe czy fałszywe (P/F) :   A. Zarodniki grzybów powstające w wyniku mitozy są diploidalne.   B. U podstawczakó i workowców występuje grzybnia dikariotyczna.  C. U niektórych grzybów kariogamia poprzedza plazmogamię.  ', 'zamknięte', '', '', 1, 4, 1),
(83, 2, 1, 'Podaj jedną cechę charakterystyczną gatunków grzybów zaklasyfikowanych do sztucznej grupy grzybow niedoskonałych.', 'otwarte', '', '', 1, 4, 1),
(84, 2, 2, 'Wyjaśnij pojęcia : kariogamia, grzybnia dikariotyczna.', 'otwarte', '', '', 2, 4, 1),
(85, 2, 3, 'Dopasuj typ protistów( I-V) do substancji, których są źródłem ( a-b).  I- okrzemki  II-brunatnice  III-krasnorosty  IV-otwornice  V-promienionóżki     a- agar  b-igiełki krzemionkowe  c-sole węglanu wapnia  d-diatomit  e-kwas alginowy', 'zamknięte', '', '', 2, 4, 1),
(86, 2, 3, 'Podane niżej protisty przyporządkuj do odpowiedniej jednostki systematycznej.                     Szkarłatnica, fitoftora, morszczyn, rzęsistek pochwowy, małżynek, ulwa sałatowa, wirczyk,rulik nadrzewny, skrętnica, pełzak czerwonki, nocoświetlik, listownica, widlik, roztoczek.             A. protisty grzybopodobne to...B. protisty roślinopodobne to...C. protisty zwierzęcopodobne to...', 'zamknięte', '', '', 4, 4, 1),
(87, 2, 3, 'Przyporządkuj podanym terminom( I-VI) ich właściwe znaczenie( a-f).   I- kolumienka   II-seta   III-chwytniki   IV-puszka   V-czepek   VI-wieczko   a- resztka rodzni, która otacza młode zarodnie u mchów   b-pasmo komórek płonnych w środkowej części puszki   c- nitkowate twory służące do pobierania wody   d- bezlistna łodyżka na której jest zarodznia   e-element przykrywający puszkę zarodniową u mchów   f- zarodnia znajdująca się na szczycie sporofitu.', 'zamknięte', '', '', 4, 4, 1),
(88, 2, 2, 'Na podstawie opisu podaj nazwę gatunkową paproci: zdanie a- u której jeden liść pełni jednocześnie obie funkcje, blaszka górna uczestniczy w procesie fotosyntezy, a blaszka dolna służy do rozmnażania.    Zdanie b- u którejwystępują dwa odrębne rodzaje  liści: sporofile i trofofile', 'otwarte', '', '', 1, 4, 1),
(89, 2, 1, 'Wyjaśnij za pomocą jednego argumentu, dlaczego paprotniki mogły osiągnąć znacznie wyższy pziom organizacji w stosunku do mszaków', 'otwarte', '', '', 1, 4, 1),
(90, 2, 1, 'wyjaśnij mechanizm otwarcia zarodni prowadzący do wysypywania się zarodników.', 'otwarte', '', '', 1, 4, 1),
(91, 2, 3, 'Wymień trzy rodzaje plechy u protistów roślinopodobnych i krótko opisz każdą ', 'otwarte', '', '', 2, 4, 1),
(92, 2, 3, 'Podaj nazwy trzech  typów zapłodnienia u protistów . Wyjaśnij na czym polega każdy z nich.', 'otwarte', '', '', 2, 4, 1),
(93, 2, 3, 'Wyjaśnij pojęcia : miksotrofy, gametangia, plemnie, lęgnie.', 'otwarte', '', '', 4, 4, 1),
(94, 2, 2, 'Wyjaśnij pojęcie symbioza i podaj przykłady protistów wchodzących w związki tego typu.', 'otwarte', '', '', 2, 4, 1),
(95, 2, 2, 'Podaj po dwa przykłady pozytywnej i negatywnej roli protistow w gospodarce człowieka.', 'otwarte', '', '', 2, 4, 1),
(96, 2, 3, 'Wymień 4 rodzaje zarodników u grzybów oraz podaj gdzie powsają i w wyniku jakiego rodzaju podziału.', 'otwarte', '', '', 4, 4, 1),
(97, 2, 3, 'Przedstaw budowę oraz środowisko i sposób życia porostów.', 'otwarte', '', '', 4, 4, 1),
(98, 2, 2, 'Podaj po dwa przykłady pozytywnej i negatywnej roli grzybow w gospodarce człowieka.', 'otwarte', '', '', 2, 4, 1),
(99, 2, 3, 'Przedstaw różne sposoby odżywiania się i oddychania u grzybów ', 'otwarte', '', '', 4, 4, 1),
(100, 2, 3, 'Podaj 4 przyklady form organizacji budowy roślin pierwotnie wodnych. Krótko opisz każdą z nich.', 'otwarte', '', '', 4, 5, 1),
(101, 2, 2, 'Waskaż czynniki, które wymusiływykształcenie tkanek przewodzących w roślinach.', 'otwarte', '', '', 2, 5, 1),
(102, 2, 3, 'Przedstaw klasyfikację tkanek stałych.', 'otwarte', '', '', 2, 5, 1),
(103, 2, 2, 'Wskaż miejsca gdzie rozmieszczone są merystemy pierwotne i wtórne w roślinie. Następnie podaj ich funkcje.', 'otwarte', '', '', 2, 5, 1),
(104, 2, 2, 'Porównaj budowę tkanki okrywającej pierwotnej i wtórnej.', 'otwarte', '', '', 1, 5, 1),
(105, 2, 2, 'Wskaż różnicę między epidermą a ryzodermą. ', 'otwarte', '', '', 1, 5, 1),
(106, 2, 3, 'scharakteryzuj  rodzaje miękiszu asymilacyjnego.', 'otwarte', '', '', 2, 5, 1),
(107, 2, 2, 'Wskaż przystosowania tkanek przewodzących, które zwiększają możliwość transportu substancji.', 'otwarte', '', '', 1, 5, 1),
(108, 2, 2, 'Oceń prawdziwość stwierdzeń dotyczących transportu w cewkach i naczyniach:  A-transport w cewkach przebiega szybciej niż w naczyniach, ponieważ załadunek zachodzi w nich bez użycia energii.   B. Transport w naczyniachzachodzi szybciej niż w cewkach, ponieważ między członami naczyń brak ścian poprzecznych.   C-Transport w naczyniach zachodzi szybciej niż w cewkach, ponieważ sciany naczyń mają liczne zgrubienia.', 'zamknięte', '', '', 1, 5, 1),
(109, 2, 2, 'Oceń prawdziwość stwierdzeń dotyczących roślin pierwotnie wodnych:  A- Liczne gatunki zielenic wchodzą w związki symbiotyczne z grzybami i współtworzą porosty     B-wielokomórkowe zielenice, podobnie jak rośliny lądowe są zbudowane z tkanek pełniących różne funkcje. Ciało tych organizmów nie jest jednak zróżnicowane na organy - ma postać plechy.', 'zamknięte', '', '', 1, 5, 1),
(110, 2, 2, 'Przyporządkuj każdemu z merystemów(A-C) jego funkcję (I-IV):   A- kallus   B-kambium   C-merystem wierzchołkowy. I- tkanka powodująca wtórny przyrost na grubość łodygi roślin dwuliściennych    II-tkanka powodująca elongacyjny wzrost roślin   III-tkanka odpowiedzialna za wytwarzanie wtórnej tkanki okrywającej    IV- tkanka występująca w miejscu zranienia rośliny.', 'zamknięte', '', '', 1, 5, 1),
(111, 2, 1, 'Podkreśl  nazwę tkanki , w której występują aparaty szparkowe : fellogen, ryzoderma, epiderma, felloderma, aerenchyma, kolenchyma', 'zamknięte', '', '', 1, 5, 1),
(112, 2, 2, 'Wymień strefy korzenia i podaj ich funkcję.', 'otwarte', '', '', 15, 5, 1),
(113, 2, 3, 'Oceń, którego organu rośliny dotyczy każda z wymienionych cech: korzenia (K) czy łodygi(Ł)  cecha A- kambium wiązkowe między ułożonymi naprzeciwlegle łykiem pierwotnymi drewnem pierwotnym.   Cecha B- śródskórnia regulująca przepływ wody w poprzek organu.  cecha C- skórka pozbawiona aparatów szparkowych.', 'zamknięte', '', '', 2, 5, 1),
(114, 2, 1, 'Wyjaśnij dlaczego w korzeniach i łodygach przyrastających na grubość jest konieczne wytworzenie korkowicy.', 'otwarte', '', '', 15, 5, 1),
(115, 2, 2, 'Podaj dwa przystosowania mchu płonnika do życia na lądzie.', 'otwarte', '', '', 1, 5, 1),
(116, 2, 2, 'Przedstaw budowę  zalążka roślin nagozalążkowych i podaj funkcję jego elementów.', 'otwarte', '', '', 2, 5, 1),
(117, 2, 3, 'Opisz proces podwójnego zapłodnienia u roślin okrytozalążkowych', 'otwarte', '', '', 4, 5, 1),
(118, 2, 1, 'Wyjaśnij dlaczego rośliny nasienne uniezależniły proces zapłodnienia od obecności wody', 'otwarte', '', '', 1, 5, 1),
(119, 2, 3, 'Przedstaw na rysunku schematycznym budowę kwiatu rośliny dwuliściennej. Podpisz elementy i podaj ich rolę.', 'otwarte', '', '', 4, 5, 1),
(120, 3, 3, 'Przyporządkuj wymienionym elementom kości(A-C) nazwy budujących je tkanek (I_IV)              A- powierzchnie stawowe   B-trzony kości   C-nasady kości     I-tkanka kostna zbita   II-tkanka kostna gąbczasta   III-tkanka chrzęstna szklista   IV-tkanka chrzęstna sprężysta', 'zamknięte', '', '', 2, 14, 1),
(121, 3, 1, 'Podkreśl te spośród wymienionych kości, w których szpik kostny u dorosłego człowieka pełni funkcje krwiotwórcze: trzon kości ramieniowej, nasada kości udowej, trzon mostka, trzon kości piszczelowej.', 'zamknięte', '', '', 1, 14, 1),
(122, 3, 1, 'Wyjaśnij, dlaczego składniki kości dorosłego człowieka podlegają wymianie.', 'otwarte', '', '', 1, 14, 1),
(123, 3, 2, 'Oceń czy zdania są prawdziwe: A-kości podudzia i stępu to przykładykości długich   B- do kości podudzia należą kości piszczelowa i strzałkowa   C-między paliczkami stopy znajdują się stawy siodełkowate', 'zamknięte', '', '', 1, 14, 1),
(124, 3, 2, 'Podaj przykłady par kości, które są połączone poprzez wymienione typy stawów(A-C)      A-staw kulisty   B-staw obrotowy   C-staw zawiasowy', 'otwarte', '', '', 1, 14, 1),
(125, 3, 3, 'Przyporządkuj podanym kościom(A-C) rodzaje połączeń, które między nimi występują   (I-IV)       A-Pierwszy i drugi kręg szyjny   B-Kości potyliczna i skroniowa dorosłego człowieka    C-kości łonowe    I- staw elipsoidalny  II-kościozrost   III-chrząstkozrost   IV-staw obrotowy', 'zamknięte', '', '', 2, 14, 1),
(126, 3, 3, 'Oceń prawdziwość zdań : A- glukoza i wolne kwasy tłuszczowe są głównymi substratami energetycznymi oddychania komórkowego   B-w trakcie zaciągania długu tlenowego glukoza jest przekształcana w kwas mlekowy bez wytworzenia ATP   C-fosfokreatyna reaguje z ADP, a produktami tej reakcji są kreatyna i ATP.', 'zamknięte', '', '', 1, 14, 1),
(127, 3, 2, 'Przyporządkuj ścisłym połączeniom kości (I-III) miejsca ich występowania w szkielecie (a-d)      I- kościozrosty   II- chrząstkozrosty   III-więzozrosty    a- połączenia między kręgami w kręgosłupie   b-połączenia między paliczkami w kończynach   c-połączenia kości występujące np w czaszce dziecka   d- połączenia kości występujące np.  w  odcinku krzyżowym kręgosłupa.', 'zamknięte', '', '', 1, 14, 1),
(128, 3, 3, 'Uzupełnij zdania (a-d) dotyczące  układu szkieletowego : A- Za procesy rozkładu kości odpowiadają komórki ……     B- staw promieniowo -nadgarstkowy to przykład stawu…….  C- włóknista błona ochraniająca kość od zewnątrz nosi nazwę ……….   D- Komórki krwi powstają w    .........    ...............     czerwonym', 'otwarte', '', '', 2, 14, 1),
(129, 3, 1, 'Zwichnięcie stawu to:  a- naciągnięcie lub naderwanie torebki stawowej i więzadeł   b-zmiana wzajemnego położenia kości tworzących staw,której towarzyszyć rozerwanie torebki stawowej    c- zwyrodnienie stawu związane z postępującą degradacją chrząstki   d-ubytek masy chrząstki spowodowany ścieraniem się powierzchni stawowych.', 'zamknięte', '', '', 1, 14, 1),
(130, 3, 2, 'Wymień kości mózgoczaszki, podaj jej funkcję.', 'otwarte', '', '', 1, 14, 1),
(131, 3, 3, 'Wymień odcinki kręgosłupa, podaj ilość kręgów i nazwę tego odcinka.', 'otwarte', '', '', 2, 14, 1),
(132, 3, 3, 'Opisz budowę i rolę szkieletu klatki piersiowej człowieka . ', 'otwarte', '', '', 2, 14, 1),
(133, 3, 2, 'Wyjaśnij jaką rolę pełnią obręcze barkowa i miedniczna w szkielecie człowieka.', 'otwarte', '', '', 1, 14, 1),
(134, 3, 2, 'Jakie cechy szkieletu kończyny górnej , zapewniają jej chwytno- manipulacyjne zdolności.', 'otwarte', '', '', 1, 14, 1),
(135, 3, 1, 'Jaką rolę  pełnią jony wapnia podczas skurczu mięśnia szkieletowego.', 'otwarte', '', '', 1, 14, 1),
(136, 3, 2, 'Oceń słuszność stwierdzenia mówiącego o tym, że przebywanie na słońcu ogranicza ryzyko zachorowania na osteoporozę. Uzasadnij swoją odpowiedź.', 'otwarte', '', '', 1, 14, 1),
(137, 3, 2, 'Przedstaw budowę mięśnia szkieletowego na schematycznym rysunku.', 'otwarte', '', '', 1, 14, 1),
(138, 3, 2, 'Podaj przykłady mięśni brzucha oraz pełnioną funkcję.', 'otwarte', '', '', 1, 14, 1),
(139, 3, 2, 'Wyjaśnij antagonistyczne działanie mięśni szkieletowych na przykładzie mięsni kończyny górnej.', 'otwarte', '', '', 2, 14, 1),
(140, 3, 2, 'Wyjaśnij pojęcia: lordoza, kifoza - podaj ich rolę w funkcjonowaniu kręgosłupa człowieka.', 'otwarte', '', '', 1, 14, 1),
(141, 3, 3, 'Przedstaw podział kości ze względu na kształt. Podaj przykłady do każdego rodzaju.', 'otwarte', '', '', 2, 14, 1),
(142, 3, 2, 'Wymień 3 funkcje szkieletu dorosłego człowieka.', 'otwarte', '', '', 1, 14, 1),
(143, 3, 3, 'Podaj przyczyny, objawy oraz profilaktykę krzywicy.', 'otwarte', '', '', 2, 14, 1),
(144, 3, 3, 'Podaj przyczyny, objawy oraz profilaktykę osteoporozy.', 'otwarte', '', '', 2, 14, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `sections`
--

CREATE TABLE `sections` (
  `id_sections` int(11) NOT NULL,
  `section_name` varchar(255) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `sections`
--

INSERT INTO `sections` (`id_sections`, `section_name`) VALUES
(1, 'Badania przyrodnicze'),
(2, 'Chemiczne podstawy życia'),
(3, 'Komórka- podstawowa jednostka życia'),
(4, 'Różnorodność wirusów bakterii, protistów i grzybów '),
(5, 'Różnorodność roślin'),
(6, 'Funkcjonowanie roślin'),
(7, 'Różnorodność bezkręgowców'),
(8, 'Funkcjonowanie roślin'),
(9, 'Różnorodność bezkręgowców'),
(10, 'Różnorodność strunowców'),
(11, 'Funkcjonowanie zwierząt'),
(12, 'Metabolizm'),
(13, 'Skóra, powłoka ciała'),
(14, 'Aparat ruchu'),
(15, 'Układ pokarmowy'),
(16, 'Obrona immunologiczna'),
(17, 'Układ wydalniczy'),
(18, 'Układ nerwowy'),
(19, 'Układ hormonalny'),
(20, 'Rozmnażanie i rozwój człowieka'),
(21, 'Choroby a zdrowie człowieka'),
(22, 'Mechanizmy dziedziczenia'),
(23, 'Biotechnologia molekularna'),
(24, 'Ekologia'),
(25, 'Ewolucja organizmów');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `registered_on` datetime NOT NULL,
  `admin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `registered_on`, `admin`) VALUES
(1, 'alcart@o2.pl', '$2b$12$WjfezTFjHK9D41tQxizwPOV1U1Vx8DbVbyKeJpZg36.RfsEw5mtwq', '2018-01-25 21:30:20', 0),
(2, 'alcart2@o2.pl', '$2b$12$iS.bYQ7YnuOTlrBMb1F9L..Ldts8nRYXNjID9w1fn5Rxeb6gLE3bm', '2018-01-25 21:36:11', 0),
(3, 'alcart2s@o2.pl', '$2b$12$tNS8uObJI.IibM9lNpxR8uZFuGH6J35mLQl7hB4wn3ErOBrZyM8Vq', '2018-01-25 21:37:30', 0),
(4, 'alcasrt2s@o2.pl', '$2b$12$Di1Ir.JMGvMPQq4lLGRyZeiih1SMkHyNVfj17isfvx3v55rYVmdAq', '2018-01-25 21:39:29', 0),
(5, 'filip@o2.pl', '$2b$12$R9CDmekMPqVI.FhQv05p4eXMfM0690nWHff9p4MuiJk95Udg4DTHC', '2018-01-26 20:53:41', 0);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `blacklist_tokens`
--
ALTER TABLE `blacklist_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `token` (`token`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `section_id_idx` (`section_id`),
  ADD KEY `creator_id` (`creator_id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id_sections`),
  ADD UNIQUE KEY `idSections_UNIQUE` (`id_sections`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `blacklist_tokens`
--
ALTER TABLE `blacklist_tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;

--
-- AUTO_INCREMENT dla tabeli `sections`
--
ALTER TABLE `sections`
  MODIFY `id_sections` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `creator_id` FOREIGN KEY (`creator_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `section_id` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id_sections`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
